
$(".btn-modal").fancybox({
    'padding'    : 0,

    btnTpl : {
        smallBtn : '<button data-fancybox-close class="callback__close" title="{{CLOSE}}"></button>'
    }
});


$('.mainSlider').slick({
    dots: true,
    infinite: true,
    arrows: true,
    autoplay: false,
    autoplaySpeed: 5000,
    slidesToShow: 1,
    slidesToScroll: 1,
    prevArrow: '<span class="slide-nav prev"></span>',
    nextArrow: '<span class="slide-nav next"></span>'
});


$('.products__slider').slick({
    dots: false,
    infinite: true,
    arrows: true,
    autoplay: false,
    autoplaySpeed: 5000,
    slidesToShow: 3,
    slidesToScroll: 1,
    prevArrow: '<span class="slide-nav prev"></span>',
    nextArrow: '<span class="slide-nav next"></span>'
});


$('.partnerSlider').slick({
    dots: false,
    infinite: true,
    arrows: true,
    autoplay: false,
    autoplaySpeed: 5000,
    slidesToShow: 4,
    slidesToScroll: 1,
    prevArrow: '<span class="slide-nav prev"></span>',
    nextArrow: '<span class="slide-nav next"></span>'
});


$('.tabs__nav li a').click(function(e) {
    e.preventDefault();
    var tab = $($(this).attr("href"));
    var box = $(this).closest('.tabs');

    $(this).closest('.tabs__nav').find('li').removeClass('active');
    $(this).closest('li').addClass('active');
    box.find('.tabs__item').removeClass('active');
    box.find(tab).addClass('active');
});



$('.nav__cat').click(function(e) {
    e.preventDefault();
    $(this).closest('.nav__elem').toggleClass('open');
    $(this).closest('.nav__elem').find('ul').slideToggle('fast');
});





<!-- maps -->

ymaps.ready(init);

function init () {
    var myMap = new ymaps.Map('map', {
            center: [55.122518569576336,61.38501849999998],
            zoom: 15,
            controls: ['smallMapDefaultSet']
        }),

        myPlacemark1 = new ymaps.Placemark([55.122518569576336,61.38501849999998], {
            balloonContent: 'A'
        }, {
            iconLayout: 'default#image',
            iconImageClipRect: [[0,0], [58, 76]],
            iconImageHref: 'img/placemark.png',
            iconImageSize: [58, 76],
            iconImageOffset: [-28, -66]
        });

    myMap.geoObjects.add(myPlacemark1);
    myMap.behaviors.disable('multiTouch');
}
<!-- -->


$(window).scroll(function() {
    if ($(this).scrollTop() > 500) {
        if ($('.btn-up').is(':hidden')) {
            $('.btn-up').css({opacity : 1}).fadeIn('slow');
        }
    } else { $('.btn-up').stop(true, false).fadeOut('fast'); }
});
$('.btn-up').click(function() {
    $('html, body').stop().animate({scrollTop : 0}, 300);
});